module.exports.sho = function (app) {
	
	var format = require('./formatter');
	
	app.get('/', function(req, res){
		res.render('index', {
			title: 'Home'
		});
	});

	app.get(
		'/:pagename', 
		function(req, res){
			var lowered = req.params.pagename.toLowerCase();
			var cappy = format.capitalize(req.params.pagename);
			res.render(lowered, {}, function(err, req) {
				if(err) {
					res.render('error', {
						title: 'Error'
					});
				} else {
					res.render(lowered, {
						title: cappy
					});
				}
			});
		}
	);
};
